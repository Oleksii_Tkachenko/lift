
<?php

// This class implements interface iLift. It works for invoking methods from client side.
// If you wish to test this class in php console in chat mode, just uncomment echo.

session_start();

interface iLift
{
    public function callTheLift($myFloor);
    public function whereIsTheLift();
    public function goToFloor($desiredFloor);
}


class Lift implements iLift
{
    private $floor;
    private $error;

    function __construct()
    {
        $this->error = false;
        $this->welcome();
    }
    private function isTheFloorExists($floor)
    {
        if ($floor < 1 || $floor > 10) {
            echo "Вы выбрали невозможный этаж! \n";
            $this->error = true;
            return false;
        } else {
            return true;
        }
    }
    public function welcome()
    {
//        echo "Добро пожаловать в 10 этажный дом! \n Лифт сейчас на {$this->floor} этаже. \n";
        if (isset($_COOKIE['floor'])) {
            $this->floor = $_COOKIE['floor'];
        }
        if (isset($_SESSION['last floor'])) {
            $this->floor = $_SESSION['last floor'];
        } else {
            $this->floor = rand(1,10);
        }
    }
    public function setRandomFloor() {
        return $this->floor = rand(1,10);
    }
    public function callTheLift($myFloor)
    {
        if (!($this->isTheFloorExists($myFloor))) return;

        if ($myFloor == $this->floor) {
            echo "Лифт уже на вашем этаже! Входите. \n";
        } else {
            $timeToArrive = (abs($this->floor - $myFloor));
            echo "Лифт едет к вам {$timeToArrive} секунд. Ожидайте... \n";
            sleep($timeToArrive);
            echo "Лифт приехал! Входите. \n";
        }
        $this->floor = $myFloor;
    }
    public function whereIsTheLift()
    {
//        echo "Лифт сейчас на {$this->floor} этаже. \n";
        return $this->floor; // comment for chat mode
    }
    public function goToFloor($desiredFloor)
    {
        echo json_encode($desiredFloor); // comment for chat mode

        if (!$this->isTheFloorExists($desiredFloor) or $this->error) return;

        $timeToArrive = (abs($this->floor - $desiredFloor));

        if ($desiredFloor == $this->floor) {
//            echo "Вы же уже здесь! \n";
        } else {
//            echo "Едем {$timeToArrive} секунд... \n";
//            sleep($timeToArrive);
//            echo "Приехали! Выходите. \n";
            $this->floor = $desiredFloor;
        }
    }
}



$lift = new Lift;

//$lift->callTheLift(10);
//$lift->goToFloor(1);
//$lift->whereIsTheLift();


$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

if ($contentType === "application/json") {
    $content = trim(file_get_contents("php://input"));
    $decoded = json_decode($content, true);

    if(isset($decoded["init"])) {
        echo json_encode($lift->setRandomFloor());
    }
    if(isset($decoded["goto"])) {
        $lift->goToFloor($decoded["goto"]);
    }
    if(isset($decoded["where"])) {
        echo json_encode($_SESSION['last floor']);
    }

}

$_SESSION['last floor'] = $lift->whereIsTheLift();

?>