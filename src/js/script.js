"use strict"

const button_start = document.querySelector(".lift_start");
const button_save_floor = document.querySelector(".save_floor");
const button_clear_floor = document.querySelector(".clear_floor");
const lift = document.querySelector(".lift");
const floors = document.querySelectorAll(".floor");
const wheel = document.querySelector(".lift__wheel");
const counterweight = document.querySelector(".lift__counterweight");
const ropeOne = document.querySelector(".lift__rope_one");
const ropeTwo = document.querySelector(".lift__rope_two");
const spinner = document.querySelector(".lds-spinner");

let liftPos = 444;
let liftFree = true;
let desPos = 1;
let wheelDeg = 0;


// Getting lift position from index.php

getLiftPosition();


// Setting button "Start lift from random floor"

button_start.addEventListener("click", (e) => {
    e.preventDefault();

    if (liftFree) {
        showTheLift(false);
        postData({"init":true})
            .then(data => setLiftPosition(data))
    }
});


// Setting button "Save floor"

button_save_floor.addEventListener("click", (e) => {
    e.preventDefault();

    if (liftFree) {
        document.cookie = `floor=${calcFloor()}; max-age=3600; path=/`
    }
});


// Setting button "Clear floor"

button_clear_floor.addEventListener("click", (e) => {
    e.preventDefault();
    document.cookie = "floor=; max-age=0; path=/";
});


// Setting "click" on each floor

floors.forEach(floor => {
    floor.onclick = (e) => {
        if (liftFree) {
            floors.forEach(floor => {
                floor.innerText = "";
            });

            e.target.innerText = "Wait...";
            e.target.style.color = "red";

            postData({"goto" : +e.target.id})
                .then(data => desPos = 497 - data * 50)
                .then(()=>liftAnimation())
        }
    }
});

async function postData(data = {}) {
    const response = await fetch('index.php', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
    });

    return await response.json();
}

function getLiftPosition() {
    postData({"where" : true})
        .then(data => setLiftPosition(data))
}

function calcFloor() {
    return (497 - liftPos) / 50;
}

function setLiftPosition(floor) {
    liftPos = 497 - floor * 50;
    desPos = liftPos;
    showTheLift(true);
    liftAnimation();
}


// Show either spinner of the lift

function showTheLift(show) {
    if (show) {
        lift.style.display = "block";
        counterweight.style.display = "block";
        ropeOne.style.display = "block";
        ropeTwo.style.display = "block";
        spinner.style.display = "none";
    } else {
        lift.style.display = "none";
        counterweight.style.display = "none";
        ropeOne.style.display = "none";
        ropeTwo.style.display = "none";
        spinner.style.display = "inline-block";
    }
}


// Setting lift animation

function liftAnimation() {
    liftFree = false;

    if (desPos < liftPos) {
        liftPos--;
        wheelDeg--;
    }
    if (desPos > liftPos) {
        liftPos++;
        wheelDeg++;
    }
    if (desPos == liftPos) {
        liftFree = true;
        floors.forEach(floor => {
            floor.innerText = "Press me!";
            floor.style.color = "black";
        });
    }

    ropeOne.style.height = 38 + liftPos + "px";
    ropeTwo.style.height = 484 - liftPos + "px";
    counterweight.style.top = 444 - liftPos + "px";
    wheel.style.transform = `rotate(${wheelDeg}deg)`;
    lift.style.top = liftPos + "px";

    if (liftPos !== desPos) {
        requestAnimationFrame(liftAnimation);
    }
}

